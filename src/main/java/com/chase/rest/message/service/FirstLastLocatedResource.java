package com.chase.rest.message.service;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class FirstLastLocatedResource {
	private Map<String, String> resourceStringDB =
			new ConcurrentHashMap<String, String>();
	
	private AtomicInteger idCounter = new AtomicInteger();
	
	public FirstLastLocatedResource() {	
		resourceStringDB.put("customer1", "Kov�cs L�szl�");
		
		resourceStringDB.put("customer2", "J�n�s P�ter");
		
		resourceStringDB.put("customer3", "Kimura Haruto");
		
		idCounter.set(4);
		
	}
	
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response postString(InputStream is)	{
		
		resourceStringDB.put( "customer" + idCounter.incrementAndGet(), is.toString()) ;
		return Response.created(URI.create(  "customer" + idCounter.get() + "") ).build();
	}
	
	// http://127.0.0.1:8080/messageREST/messageapp/dynamicdispatch/northamerica-db/customer1
	@GET
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getString(@PathParam("id") String id)	{
		
		return resourceStringDB.get(id);
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response putString(@PathParam("id") String id, InputStream is)	{
		resourceStringDB.put(  "customer" + idCounter.incrementAndGet(), is.toString()) ;
		return Response.ok(is.toString()).build();
	}
}
