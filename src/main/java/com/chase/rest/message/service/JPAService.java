package com.chase.rest.message.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import javax.xml.bind.annotation.*;

import com.chase.rest.message.model.Message;

@XmlRootElement
@XmlSeeAlso(Message.class)
public class JPAService {
	static EntityManagerFactory emf= Persistence.createEntityManagerFactory("HelloWorldPostgres");
	static EntityManager em = emf.createEntityManager();
	
	
	
	public static Message saveMessage(String newMessage)	{
		em.getTransaction().begin();
		
		TypedQuery<Message> queryOfMessages = em.createQuery("select m from Message m where m.text=:newMessage", Message.class);
		
		queryOfMessages.setParameter("newMessage", newMessage);
		
		Message output = new Message();
		output.setText(newMessage);
		
		try	{
			output = queryOfMessages.getSingleResult();
		}
		
		catch (NoResultException ex)	{
			try	{
				em.persist(output);
			}
			catch ( Exception e) {
				em.getTransaction().rollback();
				return null;
			}
			
		}
		
		
		em.getTransaction().commit();
		
		return output;
		
		/*
		List<Message> queriedMessages =  queryOfMessages.getResultList();
		
		Optional<List<Message> >  optional = Optional.ofNullable(queriedMessages);
		
		if(optional.isPresent())	{
			if(optional.get().size() != 0)	{
				em.persist(newMessage);
			}
		}
		
		if(queriedMessages != null)	{
			if(queriedMessages.size() > 0)	{
				
			}
		}
		*/
	}
	
	static public Message findMessage(String findableText)	{
		
		em.getTransaction().begin();
		
		TypedQuery<Message> queryOfMessages = em.createQuery("select m from Message m where m.text=:findableText", Message.class);
		
		queryOfMessages.setParameter("findableText", findableText);
		
		Message output = null;

		try	{
			output = queryOfMessages.getSingleResult();
		}
		
		catch (NoResultException ex)	{
			em.getTransaction().rollback();
			return null;
		}
		
		em.getTransaction().commit();
		
		return output;

	}
	
	
	public static Message findMessageById(int id)	{
		em.getTransaction().begin();
		
		Message output = null;
		
		output = em.find(Message.class, Long.valueOf(id) );
		
		em.getTransaction().commit();
		
		return output;
	}
	
	public static Message updateMessageByIdAndText(int id, String newText)	{
		
		Message messageFromDB = findMessageById(id);
		
		if(messageFromDB == null)	{
			return null;
		}
		
		em.getTransaction().begin();
		
		messageFromDB.setText(newText);
		
		em.getTransaction().commit();
		
		return messageFromDB;
		
	}
	
	public static void deleteMessageById(int id) throws NoResultException	{
		Message messageFromDB = findMessageById(id);
		
		if(messageFromDB == null)	{
			throw new NoResultException("Can't find message with id: " + id);
		}
		
		em.getTransaction().begin();
		
		em.remove(messageFromDB);
		
		em.getTransaction().commit();
	}
	
	@XmlElementRef(name="message", type=Message.class)
	@XmlElementWrapper(name="messageList")
	@XmlElement
	public static List<Message> listMessages()	{
		em.getTransaction().begin();
		
		List<Message> messages = em.createQuery("select m from Message m", Message.class).getResultList();
		
		em.getTransaction().commit();
		
		return messages;
	}
}
