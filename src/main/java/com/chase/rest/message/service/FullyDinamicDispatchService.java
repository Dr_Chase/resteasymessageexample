package com.chase.rest.message.service;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("dynamicdispatch")
public class FullyDinamicDispatchService {
	
	protected LocatedResource europe = new LocatedResource();
	
	protected FirstLastLocatedResource northamerica = new FirstLastLocatedResource();
	
	@Path("{database}-db")
	public Object getDatabase(@PathParam("database") String db) {
		if(db.equals("europe"))	{
			return europe;
		}
		
		else if(db.equals("northamerica"))	{
			return northamerica;
		}
		else return null;
	}
}
