package com.chase.rest.message.service;

import java.net.URI;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.chase.rest.message.model.Message;

@Path("/messages/")
public class MessageRestService {
	
	//http://127.0.0.1:8080/messageREST/messageapp/messages/
	@POST
	@Consumes("application/xml")
	public Response postAsXmlMessage(Message message)	{
		Message createdMessage = JPAService.saveMessage(message.getText());
		
		return Response.created(URI.create("/messages/" + createdMessage.getId() )).build();
	}
	
	@GET
	@Path("{id}")
	@Produces("application/xml")
	public Message getAsXmlMessage(@PathParam("id") int id )	{
		Message output = JPAService.findMessageById(id);
		
		if(output == null)	{
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		return output;
		
	}
	
	@PUT
	@Path("{id}")
	@Consumes("application/xml")
	public Response updateMessageWithXml(@PathParam("id") int id, Message message)	{
		Message newMessage = JPAService.updateMessageByIdAndText(id, message.getText());
		
		if(newMessage == null)	{
			throw new WebApplicationException(Response.Status.NO_CONTENT);
		}
		
		return Response.ok(newMessage).build();
		
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteMessage(@PathParam("id") int id)	{
		
		try	{
			JPAService.deleteMessageById(id);
			return Response.ok("DELETED MESSAGE WITH ID: " + id).build();
			
		}
		catch (NoResultException ex)	{
			throw new  WebApplicationException(Response.Status.NO_CONTENT);
		}
		
	}
	
	@GET
	@Produces("application/xml")
	public List<Message> getAllMessages()	{
		return JPAService.listMessages();
	}
	
	@GET
	@Produces("application/xml")
	//@Path()
	public List<Message> getSomeMessages()	{
		return JPAService.listMessages();
	}
}
