package com.chase.rest.message.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("locator")
public class SubresourceLocatorService {
	
	@Path("{stringinput}-db")
	public LocatedResource getFirstInputStringFromURI(@PathParam("stringinput") String input)	{
		
		LocatedResource resource = locateLocatedResource(input);
		
		return resource;
	}

	protected LocatedResource locateLocatedResource(String input) {
		LocatedResource output = null;
		switch (input)	{
		case "kukac":
			
			Map<Integer, String> resourceStringDBforkukac =
				new ConcurrentHashMap<Integer, String>();
			
			resourceStringDBforkukac.put(1, "kukacos alma");
			resourceStringDBforkukac.put(2, "kukacos k�rte");
			resourceStringDBforkukac.put(3, "worms armageddon");
			
			output = new LocatedResource(resourceStringDBforkukac, input);
			break;
			
		// http://127.0.0.1:8080/messageREST/messageapp/locator/jmetal-db/1
		case "jmetal":
			
			Map<Integer, String> resourceStringDBforJMetal =
				new ConcurrentHashMap<Integer, String>();
			
			resourceStringDBforJMetal.put(1, "Babymetal");
			resourceStringDBforJMetal.put(2, "Band-Maid");
			resourceStringDBforJMetal.put(3, "Ladybaby");
			
			output = new LocatedResource(resourceStringDBforJMetal, input);
			break;
			
		default:
			output = new LocatedResource();
			break;
		}
		
		return output;
	}
}
