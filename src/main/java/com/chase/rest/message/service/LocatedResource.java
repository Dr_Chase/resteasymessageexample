package com.chase.rest.message.service;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class LocatedResource {

	private Map<Integer, String> resourceStringDB =
			new ConcurrentHashMap<Integer, String>();
	
	private AtomicInteger idCounter = new AtomicInteger();
	
	private  String stringDB;

	public LocatedResource(Map<Integer, String> resourceStringDB, String dbName) {
		super();
		this.resourceStringDB = resourceStringDB;
		
		Set<Entry<Integer, String>> setOfEntries = resourceStringDB.entrySet();
		List<Integer> listOfIntegers = new ArrayList<>();
		for(Entry<Integer, String> entry : setOfEntries)	{
			listOfIntegers.add( entry.getKey() );
		}
		int maxOf = listOfIntegers.stream().mapToInt( integer -> integer).max().getAsInt();
		
		idCounter.set(maxOf + 1);
		
		stringDB = dbName;
	}
	
	public LocatedResource() {	}
	
	
	
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	public Response postString(InputStream is)	{
		
		resourceStringDB.put(idCounter.incrementAndGet(), is.toString()) ;
		return Response.created(URI.create(  idCounter.get() + "") ).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String getString(@PathParam("id") int id)	{
		
		return resourceStringDB.get(Integer.valueOf(id));
	}
	
	@PUT
	@Path("{id}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response putString(@PathParam("id") int id, InputStream is)	{
		resourceStringDB.put(idCounter.incrementAndGet(), is.toString()) ;
		return Response.ok(is.toString()).build();
	}
}
