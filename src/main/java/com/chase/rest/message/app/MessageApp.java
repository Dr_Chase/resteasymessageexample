package com.chase.rest.message.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.chase.rest.message.service.FullyDinamicDispatchService;
import com.chase.rest.message.service.MessageRestService;
import com.chase.rest.message.service.SubresourceLocatorService;

@ApplicationPath("/messageapp")
public class MessageApp extends Application {
	
	private Set<Object> singletons = new HashSet<Object>();
	
	private Set<Class<?>> empty = new HashSet<Class<?>>();
	
	public MessageApp()	{
		singletons.add(new MessageRestService());
		singletons.add(new SubresourceLocatorService());
		singletons.add(new FullyDinamicDispatchService());
	}
	
	@Override
	public Set<Class<?>> getClasses()	{
		return empty;
	}
	
	@Override
	public Set<Object> getSingletons()	{
		return singletons;
	}
}
