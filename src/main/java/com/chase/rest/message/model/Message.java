package com.chase.rest.message.model;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.*;

@Entity
@XmlRootElement(name="message")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@XmlAttribute(required = false)
	private Long id;
	
	@XmlElement
	private String text;


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}
	
	public Long getId()	{
		return id;
	}
}
